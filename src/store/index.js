import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'

Vue.use(Vuex)
Vue.use(firebase)

export const store = new Vuex.Store({
  state: {
    userRole: null,
    user: null,
    teams: {},
    userteams: {}
  },
  mutations: {
    setRole (state, payload) {
      state.userRole = payload
    },
    setUser (state, payload) {
      state.user = payload
    },
    setlevel (state, payload) {
      state.user = payload
    }
  },
  actions: {
    updatRole ({commit}, payload) {
      commit('setRole', payload.role)
    },
    setLogOutMode ({commit}) {
      commit('setRole', null)
    },
    updateUser ({commit}) {
      const currentUser = firebase.auth().currentUser
      commit('setUser', currentUser)
      firebase.firestore().collection('Users').doc(currentUser.uid).get()
        .then((snapshot) => {
          const newUser = {
            id: currentUser.uid,
            name: currentUser.displayName,
            email: currentUser.email,
            photoUrl: currentUser.photoURL,
            nivel: snapshot.get('nivel'),
            puntos: snapshot.get('puntos'),
            fechaNacimiento: snapshot.get('fechaNacimiento'),
            rol: snapshot.get('rol')
          }
          commit('setUser', newUser)
        })
    },
    clearUser ({commit}) {
      commit('setUser', null)
    }
  },
  getters: {
    getUserRole (state) {
      return state.userRole
    },
    getUser (state) {
      return state.user
    }
  }

})

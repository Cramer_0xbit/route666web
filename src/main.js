import Vue from 'vue'
import App from './App'
import router from './router'
import firebase from 'firebase'
import 'firebase/firestore'
import VueQriously from 'vue-qriously'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import navbar from './components/Navbar'
import {store} from './store'

Vue.component('navbar', navbar)
Vue.use(VueQriously)
Vue.use(Vuetify)
Vue.config.productionTip = false

let app
let firebaseconfig = {
  apiKey: 'AIzaSyCzr70FrDT_mSd3uH63SjMoIXqD3xwn8cA',
  authDomain: 'route66-644bf.firebaseapp.com',
  databaseURL: 'https://route66-644bf.firebaseio.com',
  projectId: 'route66-644bf',
  storageBucket: 'route66-644bf.appspot.com',
  messagingSenderId: '469902990532'
}
// Required for side-effects
require('firebase/firestore')
var firebaseApp = firebase.initializeApp(firebaseconfig)
export const db = firebaseApp.firestore()
const firestore = firebase.firestore()
const settings = {
  timestampsInSnapshots: true}
firestore.settings(settings)
firebase.auth().onAuthStateChanged(function (user) {
  if (!app) {
    app = new Vue({
      el: '#app',
      template: '<App/>',
      components: { App },
      router,
      store
    })
  }
})

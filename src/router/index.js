import Vue from 'vue'
import Hello from '@/components/HelloWorld'
import Router from 'vue-router'
import info from '@/components/Info.vue'
import login from '@/components/Login'
import SignUp from '@/components/SignUp'
import firebase from 'firebase'
import HelloAdmin from '@/components/HelloAdmin'
import ModificarPerfil from '@/components/modificarperfil'
import ListaUsuarios from '@/components/listaUsuarios'
import Ranking from '@/components/Ranking'
import EquiposUsuario from '@/components/EquiposUsuario.vue'
import GestionEquipo from '@/components/GestionEquipo.vue'
import GestionEquipoAdmin from '@/components/GestionEquipoAdmin.vue'
import userManagement from '@/components/UserManagement.vue'
import listaEquipos from '@/components/listaEquipos.vue'
import listaInsignias from '@/components/listaInsignias.vue'
import listaMisiones from '@/components/listaMisiones.vue'
import listaLogros from '@/components/listaLogros.vue'
import listaPremios from '@/components/listaPremios.vue'
import premiosUsuario from '@/components/premiosUsuario.vue'
import misionesUsuario from '@/components/misionesUsuario.vue'
import {store} from '../store/index.js'

Vue.use(Router)

let router = new Router({
  routes: [
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/',
      redirect: '/info'
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/info',
      name: 'info',
      component: info
    },
    {
      path: '/sign-up',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/hello',
      name: 'Hello',
      component: Hello,
      meta: {
        requiresAuth: true, clientAuth: true, adminAuth: false
      }
    },
    {
      path: '/modificarperfil',
      name: 'modificarPerfil',
      component: ModificarPerfil,
      props: true,
      meta: {
        requiresAuth: true, clientAuth: true, adminAuth: true
      }
    },
    {
      path: '/helloadmin',
      name: 'HelloAdmin',
      component: HelloAdmin,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/listausuarios',
      name: 'ListaUsuarios',
      component: ListaUsuarios,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/listaequipos',
      name: 'listaEquipos',
      component: listaEquipos,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/listainsignias',
      name: 'listaInsignias',
      component: listaInsignias,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/listamisiones',
      name: 'listaMisiones',
      component: listaMisiones,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/listalogros',
      name: 'listaLogros',
      component: listaLogros,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/listapremios',
      name: 'listaPremios',
      component: listaPremios,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/ranking',
      name: 'Ranking',
      component: Ranking,
      meta: {
        requiresAuth: true, clientAuth: true, adminAuth: false
      }
    },
    {
      path: '/equiposusuario',
      name: 'EquiposUsuario',
      component: EquiposUsuario,
      meta: {
        requiresAuth: true, clientAuth: true, adminAuth: false
      }
    },
    {
      path: '/gestionequipo',
      name: 'gestionEquipo',
      component: GestionEquipo,
      props: true,
      meta: {
        requiresAuth: true, clientAuth: true, adminAuth: false
      }
    },
    {
      path: '/gestionequipoadmin',
      name: 'GestionEquipoAdmin',
      component: GestionEquipoAdmin,
      props: true,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    },
    {
      path: '/premiosusuario',
      name: 'premiosUsuario',
      component: premiosUsuario,
      props: true,
      meta: {
        requiresAuth: true, clientAuth: true, adminAuth: false
      }
    },
    {
      path: '/misionesusuario',
      name: 'misionesUsuario',
      component: misionesUsuario,
      props: true,
      meta: {
        requiresAuth: true, clientAuth: true, adminAuth: false
      }
    },
    {
      path: '/usermanagement',
      name: 'userManagement',
      component: userManagement,
      props: true,
      meta: {
        requiresAuth: true, clientAuth: false, adminAuth: true
      }
    }

  ]
})
router.beforeEach((to, from, next) => {
  let vuexUser = store.getters.getUserRole
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  let clientAuth = to.matched.some(record => record.meta.clientAuth)
  let adminAuth = to.matched.some(record => record.meta.adminAuth)
  if (!currentUser && requiresAuth) next('login')
  if (currentUser) {
    if (!vuexUser) store.dispatch('updateUser') // evitamos sobre escrituras innecesarias
    firebase.firestore().collection('Users').doc(currentUser.uid).get().then((snapshot) => {
      var role = snapshot.get('rol')
      console.log(role)
      store.dispatch('updatRole', {role: role})
      if (adminAuth && !clientAuth && snapshot.get('rol').localeCompare('Cliente') === 0 && currentUser) {
        next('hello')
        console.log('soy un cliente')
      } else if (!adminAuth && clientAuth && snapshot.get('rol').localeCompare('Administrador') === 0 && currentUser) {
        next('ListaUsuarios')
        console.log('soy un admin')
      } else {
        next()
      }
    })
  } if (!currentUser && !requiresAuth) next()
})

export default router
